#!/bin/bash

TOKEN=$(cat "token$BITUSER")

ORGID="15911035896781928903182"

# comma separated list of ids
# get them from https://kassa.bifit.com/cashdesk/
TRADEOBJECTS="1592738493605-1170508274"  # прокат склад
# "1591878431336-776150103"  # лето киоск

CLIENTID=118187

# current unix time in milliseconds, for orderTime
# which is needed to order to be listed in bifit UI
TIMESTAMP=$(date +%s%3N)

ORDERID="@CRM_ORDER_ID@"

DATA='
{
  "onlineOrder": {
    "organizationId": "'$ORGID'",
    "tradeObjectId": "'$TRADEOBJECTS'",
    "taxSystem": "COMMON",
    "externalId": "'$ORDERID'",
    "deliveryType": "SELF",
    "paid": false,
    "orderTime": '$TIMESTAMP',
    "clientId": '$CLIENTID',
    "responsiblePersonLogin": "string",
    "totalAmount": 0,
    "currentStatusType": "NEW",
    "currentStatusTime": 0
  },
  "onlineOrderItems": []
}'

echo $DATA

curl -v -X POST https://kassa.bifit.com/cashdesk-api/v1/protected/online_orders -H "Authorization: Bearer $TOKEN" \
	-H "Content-Type: application/json" -d "$DATA"

