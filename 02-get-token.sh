#!/bin/bash

BITUSER=${BITUSER:-79857856542}
BITPASSENC=${BITPASSENC:-"dzYUVvR5gvIQVY/OPpsqO6rlNfmJ80fKtugEg7pfnLA="}

curl https://kassa.bifit.com/cashdesk-api/v1/oauth/token \
	-d username="$BITUSER" \
	-d password="$BITPASSENC" \
	-d client_id=cashdesk-rest-client \
	-d client_secret=cashdesk-rest-client \
	-d grant_type=password
