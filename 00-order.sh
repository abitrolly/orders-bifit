#!/bin/bash

# checking environment file it read successfully
env $(grep -v '^#' .env | xargs -0) bash -c 'echo -e "BITUSER: $BITUSER\nBITPASS: $BITPASS"'

# encrypting password
env $(grep -v '^#' .env | xargs -0) bash -c 'python 01-encode-pass.py <<< "$BITPASS"'

# import environment variables from .env
# (the .env should be updated manually)
set -o allexport
source .env
## echo pass from .env for comparison
echo "pass: $BITPASSENC"
set +o allexport

TOKENFILE="token$BITUSER"
AUTHFILE="$TOKENFILE.json"

# refreshing token every 12 hours
if [[ ! -f "$AUTHFILE" ]] || [[ "$(find $AUTHFILE -mmin +719)" ]]; then
  ./02-get-token.sh > "$AUTHFILE"
  echo "Refreshed $AUTHFILE"
else
  echo "Not refreshing $AUTHFILE"
fi
cat $AUTHFILE

# saving token into `tokenXXXXX` where XXXXX is username
jq -r .access_token < $AUTHFILE > $TOKENFILE

