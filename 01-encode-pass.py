#!/usr/bin/env python

# https://kassa.bifit.com/faq/obshie/auth.html

from hashlib import sha256
from base64 import b64encode

def encrypt(password):
    d = sha256(password).digest()
    #print(sha256(password).hexdigest())
    #print(d)
    b = b64encode(d)
    #print(b)
    return b.decode('utf-8')

#p = b'12345678q'
p = input('pass: ').encode('utf-8')
e = encrypt(p)
print(e)
